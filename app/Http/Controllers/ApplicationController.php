<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log, Validator, Exception, DB;

use App\User;

use App\StaticPage;

class ApplicationController extends Controller {

	/**
     * @method static_page()
     *
     * @uses used to display the static page for mobile devices
     *
     * @created Vidhya R
     *
     * @edited Vidhya R
     *
     * @param string $page_type 
     *
     * @return reidrect to the view page
     */

    public function static_page($page_type = 'terms') {

        $page_details = StaticPage::where('type' , $page_type)->first();

        return view('static-pages.index')->with('page_details', $page_details);

    }


    /**
     * @method static_pages_api()
     *
     * @uses used to get the pages
     *
     * @created Vidhya R 
     *
     * @edited Vidhya R
     *
     * @param - 
     *
     * @return JSON Response
     */

    public function static_pages_api(Request $request) {

        if($request->page_type) {

            $static_page = StaticPage::where('type' , $request->page_type)
                                ->where('status' , APPROVED)
                                ->CommonResponse()
                                ->first();

            $response_array = ['success' => true , 'data' => $static_page];

        } else {

            $static_pages = StaticPage::where('status' , APPROVED)
                                ->CommonResponse()
                                ->orderBy('title', 'asc')
                                ->get();

            $response_array = ['success' => true , 'data' => $static_pages ? $static_pages->toArray(): []];

        }

        return response()->json($response_array , 200);

    }

    /**
     * 
     */

    public function cron_clear_meetings() {

        $admin = \App\Admin::first();
        
        $timezone = $admin->timezone ?? 'Asia/Kolkata';
        
        $date = convertTimeToUSERzone(date('Y-m-d H:i:s'), $timezone);

        $delete_hour = \Setting::get('delete_video_hour') ?? 10;

        $less_than_date = date('Y-m-d H:i:s', strtotime($date." -{$delete_hour} hour"));

        $meetings = \App\Meeting::whereIn('status' , [MEETING_INITIATED, MEETING_STARTED])->where('start_time', '<=', $less_than_date)->get();

        foreach ($meetings as $key => $meeting) {

            Log::info('Change the status');

            $meeting->status = MEETING_COMPLETED;

            $meeting->save();
        }
    
    }

}
