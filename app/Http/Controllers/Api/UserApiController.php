<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\Helper;

use DB, Log, Hash, Validator, Exception, Setting;

use App\Repositories\PaymentRepository as PaymentRepo;

use App\Repositories\CommonRepository as CommonRepo;

use App\Jobs\SendEmailJob;

use App\User, App\UserCard;

use App\Subscription, App\UserSubscription;

class UserApiController extends Controller {

    protected $loginUser;

    protected $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::CommonResponse()->find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method register()
     *
     * @uses Registered user can register through manual or social login
     * 
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param Form data
     *
     * @return Json response with user details
     */
    public function register(Request $request) {

        try {

            DB::beginTransaction();

            $rules = 
                [
                    'device_type' => 'required|in:'.DEVICE_ANDROID.','.DEVICE_IOS.','.DEVICE_WEB,
                    'device_token' => 'required',
                    'login_by' => 'required|in:manual,facebook,google,apple,linkedin,instagram',
                ];

            Helper::custom_validator($request->all(), $rules);

            $allowed_social_logins = ['facebook','google','apple', 'linkedin', 'instagram'];

            if(in_array($request->login_by,$allowed_social_logins)) {

                // validate social registration fields

                $rules = [
                    'social_unique_id' => 'required',
                    'name' => 'required|max:255|min:2',
                    'email' => 'required|email|max:255',
                    'mobile' => 'digits_between:6,13',
                    'picture' => '',
                    'gender' => 'in:male,female,others',
                ];

                Helper::custom_validator($request->all(), $rules);

            } else {

                $rules = [
                        'name' => 'required|max:255',
                        'email' => 'required|email|max:255|min:2',
                        'password' => 'required|min:6',
                        'picture' => 'mimes:jpeg,jpg,bmp,png',
                    ];

                Helper::custom_validator($request->all(), $rules);

                // validate email existence

                $rules =
                    [
                        'email' => 'unique:users,email',
                    ];

                Helper::custom_validator($request->all(), $rules);

            }

            $user = User::where('email' , $request->email)->first();

            $send_email = NO;

            // Creating the user

            if(!$user) {

                $user = new User;

                register_mobile($request->device_type);

                $send_email = YES;

                $user->picture = asset('placeholder.jpg');

                $user->registration_steps = 1;

            } else {

                if(in_array($user->status , [USER_PENDING , USER_DECLINED])) {

                    throw new Exception(api_error(1000), 1000);
                
                }

            }

            if($request->has('name')) {

                $user->name = $request->name;

            }

            if($request->has('email')) {

                $user->email = $request->email;

            }

            if($request->has('mobile')) {

                $user->mobile = $request->mobile;

            }

            if($request->has('password')) {

                $user->password = Hash::make($request->password ?: "123456");

            }

            $user->gender = $request->has('gender') ? $request->gender : "male";

            $user->payment_mode = COD;

            $user->token = Helper::generate_token();

            $user->token_expiry = Helper::generate_token_expiry();

            $check_device_exist = User::where('device_token', $request->device_token)->first();

            if($check_device_exist) {

                $check_device_exist->device_token = "";

                $check_device_exist->save();
            }

            $user->device_token = $request->device_token ?: "";

            $user->device_type = $request->device_type ?: DEVICE_WEB;

            $user->login_by = $request->login_by ?: 'manual';

            $user->social_unique_id = $request->social_unique_id ?: '';

            // Upload picture

            if($request->login_by == "manual") {

                if($request->hasFile('picture')) {

                    $user->picture = Helper::upload_file($request->file('picture') , PROFILE_PATH_USER);

                }

            } else {

                $user->is_verified = USER_EMAIL_VERIFIED; // Social login

                $user->picture = $request->picture ?: $user->picture;

            }   

            if($user->save()) {

                // $user->is_verified = USER_EMAIL_VERIFIED;

                $user->save();

                // Send welcome email to the new user:

                if($send_email) {


                    if($user->login_by == 'manual' && Setting::get('is_account_email_verification') == YES) {

                        Helper::send_verification_email($user);

                    }

                }

                if(in_array($user->status , [USER_DECLINED , USER_PENDING])) {
                
                    $response = ['success' => false , 'error' => api_error(1000) , 'error_code' => 1000];

                    DB::commit();

                    return response()->json($response, 200);
               
                }

                // if($user->is_verified == USER_EMAIL_VERIFIED) {

                	$data = User::CommonResponse()->find($user->id);

                    $response_array = ['success' => true,'messages' => api_success(1001), 'data' => $data];

                // } else {

                    // $response_array = ['success'=>false, 'error' => api_error(1001), 'error_code'=>1001];

                    // DB::commit();

                    // return response()->json($response_array, 200);

                // }

            } else {

                throw new Exception(api_error(103), 103);

            }

            DB::commit();

            return response()->json($response_array, 200);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method login()
     *
     * @uses Registered user can login using their email & password
     * 
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Email & Password
     *
     * @return Json response with user details
     */
    public function login(Request $request) {

        try {

            DB::beginTransaction();

            $basic_validator = Validator::make($request->all(),
                [
                    'device_token' => 'required',
                    'device_type' => 'required|in:'.DEVICE_ANDROID.','.DEVICE_IOS.','.DEVICE_WEB,
                    'login_by' => 'required|in:manual,facebook,google,apple,linkedin,instagram',
                ]
            );

            if($basic_validator->fails()){

                $error = implode(',', $basic_validator->messages()->all());

                throw new Exception($error , 101);

            }

            /** Validate manual login fields */

            $manual_validator = Validator::make($request->all(),
                [
                    'email' => 'required|email',
                    'password' => 'required',
                ]
            );

            if($manual_validator->fails()) {

                $error = implode(',', $manual_validator->messages()->all());

            	throw new Exception($error , 101);

            }

            $user = User::where('email', '=', $request->email)->first();

            $email_active = DEFAULT_TRUE;

            // Check the user details 

            if(!$user) {

            	throw new Exception(api_error(1002), 1002);

            }

            // check the user approved status

            if($user->status != USER_APPROVED) {

            	throw new Exception(api_error(1000), 1000);

            }

            // if(Setting::get('is_account_email_verification') == YES) {

                if(!$user->is_verified) {

                    Helper::send_verification_email($user);

                    $email_active = DEFAULT_FALSE;

                }

            // }

            if(!$email_active) {

    			throw new Exception(api_error(1001), 1001);
            }

            if(Hash::check($request->password, $user->password)) {

                // Generate new tokens
                
                $user->token = Helper::generate_token();

                $user->token_expiry = Helper::generate_token_expiry();
                
                // Save device details

                $check_device_exist = User::where('device_token', $request->device_token)->first();

                if($check_device_exist) {

                    $check_device_exist->device_token = "";
                    
                    $check_device_exist->save();
                }

                $user->device_token = $request->device_token ?? $user->device_token;

                $user->device_type = $request->device_type ?? $user->device_type;

                $user->login_by = $request->login_by ?? $user->login_by;

                $user->save();

                $data = User::CommonResponse()->find($user->id);

                $response_array = array('success' => true, 'message' => Helper::success_message(101), 'data' => $data);

            } else {

				throw new Exception(api_error(102), 102);
                
            }

            DB::commit();

            return response()->json($response_array, 200);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }
 
    /**
     * @method forgot_password()
     *
     * @uses If the user forgot his/her password he can hange it over here
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - Email id
     *
     * @return send mail to the valid user
     */
    
    public function forgot_password(Request $request) {

        try {

            DB::beginTransaction();

            // Check email configuration and email notification enabled by admin

            if(Setting::get('is_email_notification') != YES ) {

                throw new Exception(api_error(106), 106);
                
            }
            
            $rules = [
                'email' => 'required|email|exists:users,email',
            ]; 

            $custom_errors = [
                'exists' => 'The :attribute doesn\'t exists',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            $user = User::where('email' , $request->email)->first();

            if(!$user) {

                throw new Exception(api_error(1002), 1002);
            }

            if($user->login_by != 'manual') {

                throw new Exception(api_error(118), 118);
                
            }

            // check email verification

            if($user->is_verified == USER_EMAIL_NOT_VERIFIED) {

                throw new Exception(api_error(1001), 1001);
            }

            // Check the user approve status

            if(in_array($user->status , [USER_DECLINED , USER_PENDING])) {
                throw new Exception(api_error(1000), 1000);
            }

            $new_password = Helper::generate_password();

            $user->password = Hash::make($new_password);

            $email_data['subject'] = tr('user_forgot_email_title' , Setting::get('site_name'));

            $email_data['email']  = $user->email;

            $email_data['password'] = $new_password;

            $email_data['page'] = "emails.users.forgot-password";

            $this->dispatch(new SendEmailJob($email_data));

            if(!$user->save()) {

                throw new Exception(api_error(103));

            }

            DB::commit();

            return $this->sendResponse(api_success(102), $success_code = 102, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    }

    /**
     * @method change_password()
     *
     * @uses To change the password of the user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - Password & confirm Password
     *
     * @return json response of the user
     */
    public function change_password(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'password' => 'required|confirmed|min:6',
                'old_password' => 'required|min:6',
            ]; 

            Helper::custom_validator($request->all(), $rules, $custom_errors =[]);

            $user = User::find($request->id);

            if(!$user) {

                throw new Exception(api_error(1002), 1002);
            }

            if($user->login_by != "manual") {

                throw new Exception(api_error(118), 118);
                
            }

            if(Hash::check($request->old_password,$user->password)) {

                $user->password = Hash::make($request->password);
                
                if($user->save()) {

                    DB::commit();

                    $email_data['subject'] = tr('change_password_email_title' , Setting::get('site_name'));

                    $email_data['email']  = $user->email;

                    $email_data['page'] = "emails.users.change-password";

                    $this->dispatch(new SendEmailJob($email_data));

                    return $this->sendResponse(Helper::success_message(104), $success_code = 104, $data = []);
                
                } else {

                    throw new Exception(api_error(103), 103);   
                }

            } else {

                throw new Exception(api_error(108) , 108);
            }

            

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /** 
     * @method profile()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function profile(Request $request) {

        try {

            $user = User::where('id' , $request->id)->CommonResponse()->first();

            if(!$user) { 

                throw new Exception(api_error(1002) , 1002);
            }

            $card_last_four_number = "";

            if($user->user_card_id) {

                $card = UserCard::find($user->user_card_id);

                if($card) {

                    $card_last_four_number = $card->last_four;

                }

            }

            $data = $user->toArray();

            $data['card_last_four_number'] = $card_last_four_number;

            //$overall_rating = ProviderRating::where('user_id', $request->id)->avg('rating');

            // $data['overall_rating'] =   $overall_rating ? intval($overall_rating) : 0;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }
 
    /**
     * @method update_profile()
     *
     * @uses To update the user details
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param objecct $request : User details
     *
     * @return json response with user details
     */
    public function update_profile(Request $request) {

        try {

            DB::beginTransaction();
            
            $validator = Validator::make($request->all(),
                [
                    'name' => 'required|max:255',
                    'email' => 'email|unique:users,email,'.$request->id.'|max:255',
                    'mobile' => 'digits_between:6,13',
                    // 'picture' => 'mimes:jpeg,bmp,png',
                    'gender' => 'nullable|in:male,female,others',
                    'device_token' => '',
                    'description' => ''
                ]);

            if($validator->fails()) {

                // Error messages added in response for debugging

                $error = implode(',',$validator->messages()->all());
             
                throw new Exception($error , 101);
                
            }

            $user = User::find($request->id);

            if(!$user) { 

                throw new Exception(api_error(1002) , 1002);
            }

            $user->name = $request->name ? $request->name : $user->name;
            
            if($request->has('email')) {

                $user->email = $request->email;
            }

            $user->mobile = $request->mobile ?: $user->mobile;

            $user->gender = $request->gender ?: $user->gender;

            $user->description = $request->description ?: '';

            // Upload picture
            if($request->hasFile('picture') != "") {

                Helper::delete_file($user->picture, COMMON_FILE_PATH); // Delete the old pic

                $user->picture = Helper::upload_file($request->file('picture') , COMMON_FILE_PATH);

            }

            if($user->save()) {

            	$data = User::CommonResponse()->find($user->id);

                DB::commit();

                return $this->sendResponse($message = tr('user_profile_update_success'), $success_code = 200, $data);

            } else {    

        		throw new Exception(api_error(103), 103);
            }

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method delete_account()
     * 
     * @uses Delete user account based on user id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - Password and user id
     *
     * @return json with boolean output
     */

    public function delete_account(Request $request) {

        try {

            DB::beginTransaction();

            $request->request->add([ 
                'login_by' => $this->loginUser ? $this->loginUser->login_by : "manual",
            ]);

            $validator = Validator::make($request->all(),
                [
                    'password' => 'required_if:login_by,manual',
                ]);

            if($validator->fails()) {

                $error = implode(',',$validator->messages()->all());
             
                throw new Exception($error , 101);
                
            }

            $user = User::find($request->id);

            if(!$user) {

            	throw new Exception(api_error(1002), 1002);
                
            }

            // The password is not required when the user is login from social. If manual means the password is required

            if($user->login_by == 'manual') {

                if(!Hash::check($request->password, $user->password)) {

                    $is_delete_allow = NO ;

                    $error = api_error(104);
         
                    throw new Exception($error , 104);
                    
                }
            
            }

            if($user->delete()) {

                DB::commit();

                // @todo 

                $message = tr('account_delete_success');

                return $this->sendResponse($message, $success_code = 200, $data = []);

            } else {

                // @todo 

            	throw new Exception("Error Processing Request", 101);
            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

	}

    /**
     * @method logout()
     *
     * @uses Logout the user
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param 
     * 
     * @return
     */
    public function logout(Request $request) {

        // @later no logic for logout

        return $this->sendResponse(Helper::success_message(106), 106);

    }

    /**
     * @method cards_list()
     *
     * @uses get the user payment mode and cards list
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer id
     * 
     * @return
     */

    public function cards_list(Request $request) {

        try {

            $user_cards = UserCard::where('user_id' , $request->id)->select('id as user_card_id' , 'customer_id' , 'last_four' ,'card_holder_name', 'card_token' , 'is_default' )->get();

            // $data = $user_cards ? $user_cards : []; 

            $card_payment_mode = $payment_modes = [];

            $card_payment_mode['name'] = "Card";

            $card_payment_mode['payment_mode'] = "card";

            $card_payment_mode['is_default'] = 1;

            array_push($payment_modes , $card_payment_mode);

            $data['payment_modes'] = $payment_modes;   

            $data['cards'] = $user_cards ? $user_cards : []; 

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }
    
    /**
     * @method cards_add()
     *
     * @uses used to add card to the user
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param card_token
     * 
     * @return JSON Response
     */
    public function cards_add(Request $request) {

        try {

            if(Setting::get('stripe_secret_key')) {

                \Stripe\Stripe::setApiKey(Setting::get('stripe_secret_key'));

            } else {

                throw new Exception(api_error(121), 121);

            }

            // Validation start

            $rules = ['card_token' => 'required'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end
            
            $user = User::find($request->id);

            if(!$user) {

                throw new Exception(api_error(1002), 1002);
                
            }

            DB::beginTransaction();

            // Get the key from settings table
            
            $customer = \Stripe\Customer::create([
                    "card" => $request->card_token,
                    "email" => $user->email,
                    "description" => "Customer for ".Setting::get('site_name'),
                ]);

            if($customer) {

                $customer_id = $customer->id;

                $card = new UserCard;

                $card->user_id = $request->id;

                $card->customer_id = $customer_id;

                $card->card_token = $customer->sources->data ? $customer->sources->data[0]->id : "";

                $card->card_type = $customer->sources->data ? $customer->sources->data[0]->brand : "";

                $card->last_four = $customer->sources->data[0]->last4 ? $customer->sources->data[0]->last4 : "";

                $card->card_holder_name = $request->card_holder_name ?: $this->loginUser->name;

                // Check is any default is available

                $check_card = UserCard::where('user_id',$request->id)->count();

                $card->is_default = $check_card ? NO : YES;

                if($card->save()) {

                    if($user) {

                        $user->user_card_id = $check_card ? $user->user_card_id : $card->id;

                        $user->save();
                    }

                    $data = UserCard::where('id' , $card->id)->CommonResponse()->first();

                    DB::commit();

                    return $this->sendResponse(api_success(105), 105, $data);

                } else {

                    throw new Exception(api_error(114), 114);
                    
                }
           
            } else {

                throw new Exception(api_error(121) , 121);
                
            }

        } catch(Stripe_CardError | Stripe_InvalidRequestError | Stripe_AuthenticationError | Stripe_ApiConnectionError | Stripe_Error $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode() ?: 101);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode() ?: 101);
        }

    }

    /**
     * @method cards_delete()
     *
     * @uses delete the selected card
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer user_card_id
     * 
     * @return JSON Response
     */

    public function cards_delete(Request $request) {

        try {

            DB::beginTransaction();

            // validation start

            $rules = [
                    'user_card_id' => 'required|integer|exists:user_cards,id,user_id,'.$request->id,
                    ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // validation end

            $user = User::find($request->id);

            if(!$user) {

                throw new Exception(api_error(1002), 1002);
            }

            UserCard::where('id', $request->user_card_id)->delete();

            if($user->payment_mode = CARD) {

                // Check he added any other card

                if($check_card = UserCard::where('user_id' , $request->id)->first()) {

                    $check_card->is_default =  DEFAULT_TRUE;

                    $user->user_card_id = $check_card->id;

                    $check_card->save();

                } else { 

                    $user->payment_mode = COD;

                    $user->user_card_id = DEFAULT_FALSE;
                
                }
           
            }

            // Check the deleting card and default card are same

            if($user->user_card_id == $request->user_card_id) {

                $user->user_card_id = DEFAULT_FALSE;

                $user->save();
            }
            
            $user->save();
                
            DB::commit();

            return $this->sendResponse(api_success(109), 109, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method cards_default()
     *
     * @uses update the selected card as default
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer id
     * 
     * @return JSON Response
     */
    public function cards_default(Request $request) {

        try {

            DB::beginTransaction();

            // validation start

            $rules = [
                    'user_card_id' => 'required|integer|exists:user_cards,id,user_id,'.$request->id,
                    ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // validation end

            $user = User::find($request->id);

            if(!$user) {

                throw new Exception(api_error(1002), 1002);
            }
        
            $old_default_cards = UserCard::where('user_id' , $request->id)->where('is_default', YES)->update(['is_default' => NO]);

            $user_cards = UserCard::where('id' , $request->user_card_id)->update(['is_default' => YES]);

            $user->user_card_id = $request->user_card_id;

            $user->save();

            DB::commit();

            return $this->sendResponse(api_success(201), 201);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    } 

    /**
     * @method payment_mode_default()
     *
     * @uses update the selected card as default
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer id
     * 
     * @return JSON Response
     */
    public function payment_mode_default(Request $request) {

        Log::info("payment_mode_default");

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), [

                'payment_mode' => 'required',

            ]);

            if($validator->fails()) {

                $error = implode(',',$validator->messages()->all());

                throw new Exception($error, 101);

            }

            $user = User::find($request->id);

            $user->payment_mode = $request->payment_mode ?: CARD;

            $user->save();           

            DB::commit();

            return $this->sendResponse($message = "Mode updated", $code = 200, $data = ['payment_mode' => $request->payment_mode]);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    } 

    /**
     * @method notification_settings()
     *
     * @uses To enable/disable notifications of email / push notification
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param - 
     *
     * @return JSON Response
     */
    public function notification_settings(Request $request) {

        try {

            DB::beginTransaction();

            $validator = Validator::make(
                $request->all(),
                array(
                    'status' => 'required|numeric',
                    'type'=>'required|in:'.EMAIL_NOTIFICATION.','.PUSH_NOTIFICATION
                )
            );

            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);

            }
                
            $user = User::find($request->id);

            if($request->type == EMAIL_NOTIFICATION) {

                $user->email_notification_status = $request->status;

            }

            if($request->type == PUSH_NOTIFICATION) {

                $user->push_notification_status = $request->status;

            }

            $user->save();

            $message = $request->status ? Helper::success_message(206) : Helper::success_message(207);

            $data = ['id' => $user->id , 'token' => $user->token];

            $response_array = [
                'success' => true ,'message' => $message, 
                'email_notification_status' => (int) $user->email_notification_status,  // Don't remove int (used ios)
                'push_notification_status' => (int) $user->push_notification_status,    // Don't remove int (used ios)
                'data' => $data
            ];
                
            
            DB::commit();

            return response()->json($response_array , 200);

        } catch (Exception $e) {

            DB::rollback();

            $error = $e->getMessage();

            $code = $e->getCode();

            $response_array = ['success'=>false, 'error'=>$error, 'error_code'=>$code];

            return response()->json($response_array);
        }

    }

    /**
     * @method configurations()
     *
     * @uses used to get the configurations for base products
     *
     * @created Vithya R
     *
     * @updated - 
     *
     * @param - 
     *
     * @return JSON Response
     */
    public function configurations(Request $request) {

        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required|exists:users,id',
                'token' => 'required',

            ]);

            if($validator->fails()) {

                $error = implode(',',$validator->messages()->all());

                throw new Exception($error, 101);

            }

            $config_data = $data = [];

            $payment_data['is_stripe'] = 1;

            $payment_data['stripe_publishable_key'] = Setting::get('stripe_publishable_key') ?: "";

            $payment_data['stripe_secret_key'] = Setting::get('stripe_secret_key') ?: "";

            $payment_data['stripe_secret_key'] = Setting::get('stripe_secret_key') ?: "";

            $data['payments'] = $payment_data;

            $data['urls']  = [];

            $url_data['base_url'] = envfile("APP_URL") ?: "";

            $url_data['chat_socket_url'] = Setting::get("chat_socket_url") ?: "";

            $data['urls'] = $url_data;

            $notification_data['FCM_SENDER_ID'] = "";

            $notification_data['FCM_SERVER_KEY'] = $notification_data['FCM_API_KEY'] = "";

            $notification_data['FCM_PROTOCOL'] = "";

            $data['notification'] = $notification_data;

            $data['site_name'] = Setting::get('site_name');

            $data['site_logo'] = Setting::get('site_logo');

            $data['currency'] = Setting::get('currency');

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method subscriptions_index()
     *
     * @uses To display all the subscription plans
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function subscriptions_index(Request $request) {

        try {

            $subscriptions = Subscription::Approved()->orderBy('amount', 'asc')->get();

            return $this->sendResponse($message = '' , $code = '', $subscriptions);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method subscriptions_view()
     *
     * @uses get the selected subscription details
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param integer $subscription_id
     *
     * @return JSON Response
     */
    public function subscriptions_view(Request $request) {

        try {

            $rules = [
                'subscription_id' => 'required|exists:subscriptions,id',$request->subscription_id,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $subscription = Subscription::BaseResponse()->where('subscriptions.status' , APPROVED)->where('subscriptions.id', $request->subscription_id)->first();

            if(!$subscription) {
                throw new Exception(api_error(135), 135);   
            }

            return $this->sendResponse($message = '' , $code = '', $subscription);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method subscriptions_history()
     *
     * @uses get the selected subscription details
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param integer $subscription_id
     *
     * @return JSON Response
     */
    public function subscriptions_history(Request $request) {

        try {

            $user_subscriptions = UserSubscription::BaseResponse()->where('user_id' , $request->id)->skip($this->skip)->take($this->take)->orderBy('user_subscriptions.id', 'desc')->get();

            foreach ($user_subscriptions as $key => $value) {

                $value->plan_text = formatted_plan($value->plan ?? 0);

                $value->expiry_date = common_date($value->expiry_date, $this->timezone ?? '', 'd M Y');

                $value->no_of_users_formatted = no_of_users_formatted($value->no_of_users);

                $value->no_of_hrs_formatted = no_of_hrs_formatted($value->no_of_hrs, $value->no_of_hrs_type);
            
            }

            return $this->sendResponse($message = '' , $code = '', $user_subscriptions);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /** 
     * @method subscriptions_payment_by_card()
     *
     * @uses pay for subscription using paypal
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function subscriptions_payment_by_card(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'subscription_id' => 'required|exists:subscriptions,id',
                    ];

            $custom_errors = ['subscription_id' => api_error(151)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);
            
            // Validation end

           // Check the subscription is available

            $subscription = Subscription::where('id',  $request->subscription_id)
                                    ->Approved()
                                    ->first();

            if(!$subscription) {

                throw new Exception(api_error(161), 161);
                
            }

            $request->request->add(['payment_mode' => CARD]);

            $total = $user_pay_amount = $subscription->amount ?? 0.00;


            $request->request->add([
                'total' => $total, 
                'user_pay_amount' => $user_pay_amount,
                'paid_amount' => $user_pay_amount,
            ]);

            if($user_pay_amount > 0) {

                // Check the user have the cards

                $card = \App\UserCard::where('user_id', $request->id)->where('is_default', YES)->first();

                // If the user doesn't have cards means the payment will switch to COD

                if(!$card) {

                    throw new Exception(api_error(163), 163); 

                }

                $request->request->add(['customer_id' => $card->customer_id]);
                
                $card_payment_response = PaymentRepo::subscriptions_payment_by_stripe($request, $subscription)->getData();

                if($card_payment_response->success == false) {

                    throw new Exception($card_payment_response->error, $card_payment_response->error_code);
                    
                }

                $card_payment_data = $card_payment_response->data;

                $request->request->add(['paid_amount' => $card_payment_data->paid_amount, 'payment_id' => $card_payment_data->payment_id, 'paid_status' => $card_payment_data->paid_status]);

            }

            $payment_response = PaymentRepo::subscriptions_payment_save($request, $subscription)->getData();

            if($payment_response->success) {
                
                DB::commit();

                $code = 111;

                return $this->sendResponse(api_success($code), $code, $payment_response->data);

            } else {

                throw new Exception($payment_response->error, $payment_response->error_code);
                
            }
        
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method subscriptions_payment_by_paypal()
     *
     * @uses pay for subscription using paypal
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function subscriptions_payment_by_paypal(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'subscription_id' => 'required|exists:subscriptions,id',
                    'payment_id' => 'required',
                    ];

            $custom_errors = ['subscription_id' => api_error(151)];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

           // Check the subscription is available

            $subscription = Subscription::where('id',  $request->subscription_id)
                                    ->Approved()
                                    ->first();

            if(!$subscription) {

                throw new Exception(api_error(161), 161);
                
            }

            $request->request->add(['payment_mode' => PAYPAL]);

            $total = $user_pay_amount = $subscription->amount ?? 0.00;

            $request->request->add([
                'total' => $total, 
                'user_pay_amount' => $user_pay_amount,
                'paid_amount' => $user_pay_amount,
            ]);

            $payment_response = PaymentRepo::subscriptions_payment_save($request, $subscription)->getData();

            if($payment_response->success) {
                
                DB::commit();

                $code = 111;

                return $this->sendResponse(api_success($code), $code, $payment_response->data);

            } else {

                throw new Exception($payment_response->error, $payment_response->error_code);
                
            }
        
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method email_verification()
     *
     * @uses User Email Verification
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function email_verification(Request $request) {

        try {

            $rules = [
                'verification_code' => 'required',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $user = User::find($request->id);

            if($user->is_verified == USER_EMAIL_VERIFIED) {
                
                throw new Exception(api_error(1010), 1010);
           
            }

            if ($request->verification_code === $user->verification_code ) { 

                $user->is_verified = USER_EMAIL_VERIFIED;

                $user->save();

                $user = User::CommonResponse()->find($request->id);
                
                return $this->sendResponse($message = api_success(101), $code = 101, $user);

            } else {

                $user->token = Helper::generate_token();

                $user->token_expiry = Helper::generate_token_expiry();

                $user->save();

                Helper::send_verification_email($user);

                $user = User::CommonResponse()->find($request->id);

                return $this->sendResponse($message = api_success(1001), $code = 1001, $user);
            }

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }


}
