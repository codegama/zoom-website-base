<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'plan', 'amount', 'no_of_users', 'no_of_hrs'];

    protected $appends = ['plan_text', 'subscription_amount_formatted', 'no_of_users_formatted', 'no_of_hrs_formatted', 'currency', 'subscription_id','subscribers_count'];

    public function getSubscriptionIdAttribute() {

        return $this->id;
    }

    public function getPlanTextAttribute() {

        return formatted_plan($this->plan);
    }

    public function getSubscriptionAmountFormattedAttribute() {

        return formatted_amount($this->amount);
    }

    public function getNoOfUsersFormattedAttribute() {

        return no_of_users_formatted($this->no_of_users);
    }

    public function getNoOfHrsFormattedAttribute() {

        return no_of_hrs_formatted($this->no_of_hrs, $this->no_of_hrs_type);
    }

    public function getCurrencyAttribute() {

        return \Setting::get('currency' , '$');
    }


    public function getSubscribersCountAttribute() {

        return $this->userSubscription()->count();
    }

    /**
	 * Save the unique ID 
	 *
	 *
	 */
    public function setUniqueIdAttribute($value){

		$this->attributes['unique_id'] = uniqid(str_replace(' ', '-', $value));
	}

    /**
     * Get the UserSubscription record associated with the Subscription.
     */
    public function userSubscription() {
        
        return $this->hasMany(UserSubscription::class, 'subscription_id');
    }

    /**
     * Get the User record associated with the Subscription.
     */
    public function user() {
        
        return $this->hasMany(User::class, 'user_id');
    }

	/**
     * Scope a query to basic subscription details
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBaseResponse($query) {

        $currency = \Setting::get('currency' , '$');

        return $query->select('subscriptions.id as subscription_id', 
                'subscriptions.title', 
                'subscriptions.description', 
                'subscriptions.plan',
                'subscriptions.amount', 
                'subscriptions.popular_status', 
                'subscriptions.status', 
                'subscriptions.created_at' , 
                \DB::raw("'$currency' as currency")
                );
    }

    /**
     * Scope a query to basic subscription details
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        return $query->where('subscriptions.status', APPROVED);
    }

    public static function boot() {
       
        parent::boot();

        static::deleting(function ($model) {

            foreach ($model->userSubscription as $key => $user_subscription) {
                
                $user_subscription->delete();                
            }

        });

    }
}
