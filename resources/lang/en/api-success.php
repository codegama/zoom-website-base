<?php

return [

    1001 => 'Please verify your email address!!',

    101 => 'Successfully loggedin!!',

    102 => 'Mail sent successfully',

    103 => 'Your account deleted successfully.',

    104 => 'Your password changed successfully.',

    106 => 'Logged out successfully',

    105 => 'The card added successfully',

    108 => 'The card changed to default.',

    109 => 'The card deleted',

    110 => 'The card marked as default.',
    
    111 => 'Your payment done successfully.',

    112 => 'Meeting Started.',

    113 => 'Meeting Ended.',

    114 => 'Meeting allowed',
    
];
