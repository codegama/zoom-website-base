<form action="{{route('admin.meetings.index')}}" method="GET" role="search">

    @if(request()->search_key != '')
        <div class="col-12 pb-3">
            <p class="text-muted"> {{ tr('search_results_for')}} <b>{{ request()->search_key }}</b></p>
        </div>
    @endif

<div class="row pt-2 pb-2">

    <div class="col-3">

        <select class="form-control select2" name="status">

            <option class="select-color" value="">{{tr('select_status')}}</option>

            <option class="select-color" value="{{MEETING_INITIATED}}" @if(Request::get('status') == MEETING_INITIATED && Request::get('status')!='') selected @endif>{{tr('meeting_initiated')}}</option>

            <option class="select-color" value="{{MEETING_STARTED}}" @if(Request::get('status') == MEETING_STARTED && Request::get('status')!='') selected @endif>{{tr('meeting_started')}}</option>

            <option class="select-color" value="{{MEETING_COMPLETED}}" @if(Request::get('status') == MEETING_COMPLETED && Request::get('status')!='') selected @endif>{{tr('meeting_completed')}}</option>

        </select>

    </div>


    <div class="col-9">

        <div class="input-group">
            <input type="text" class="form-control" name="search_key" placeholder="{{tr('meetings_search_placeholder')}}"> <span class="input-group-btn">

                &nbsp

                <button type="submit" class="btn btn-primary btn-width">
                    {{tr('search')}}
                </button>

                <a class="btn btn-primary" href="{{route('admin.meetings.index')}}">{{tr('clear')}}
                </a>
                </span>
            </div>

        </div>

    </div>

</form>