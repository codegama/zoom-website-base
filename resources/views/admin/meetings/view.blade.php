@extends('layouts.admin')

@section('page_header',tr('meetings'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.meetings.index')}}">{{tr('meetings')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_meetings')}}</li>
@endsection

@section('content')

<div class="card">

	<div class="card-header bg-info"> 

		<h4 class="m-b-0 text-white">{{tr('view_meetings')}}
            <div class="pull-right">   
                @if(Setting::get('is_demo_control_enabled') == NO)
                                                    
                    <a class="btn btn-danger" href="{{route('admin.meetings.delete', ['meeting_id' => $meeting_details->id,'page'=>'$page'])}}" onclick="return confirm(&quot;{{tr('meeting_delete_confirmation', $meeting_details->meeting_name)}}&quot;);"><i class="mdi mdi-delete mdi-20px"> </i>{{tr('delete')}} </a>

                    @if($meeting_details->status == MEETING_INITIATED || $meeting_details->status == MEETING_STARTED)

                         <a class="btn btn-warning" href="{{ route('admin.meetings.end', ['meeting_id' => $meeting_details->id]) }}" onclick="return confirm(&quot;{{tr('meeting_end_confirmation', $meeting_details->meeting_name)}}&quot;);"><i class="mdi mdi-close-circle"> </i>{{tr('end_meeting')}} </a>
                    @endif

                @else

                    <a class="btn btn-danger" href="javascript:;"><i class="mdi mdi-delete mdi-20px"> </i>{{tr('delete')}}</a>

                    @if($meeting_details->status == MEETING_INITIATED || $meeting_details->status == MEETING_STARTED)

                         <a class="btn btn-danger" href="javascript:;"><i class="mdi mdi-close-circle"> </i>{{tr('end_meeting')}}</a>

                    @endif

                @endif
            </div>
        </h4>

	</div>

    <div class="card-body">

    	<div class="row">


            <div class="col-12">

                <!-- Card content -->
                <div class="card-body">

                    <div class="template-demo {{ request()->type == 'view_members' ? 'display-none' : '' }} ">

                        <table class="table">

                          <tbody>

                            <tr>
                                <td><b>{{ tr('meeting_name') }}</b></td>
                                <td><div >{{$meeting_details->meeting_name}}</div></td>
                            </tr>

                            <tr>
                                <td><b>{{ tr('username') }}</b></td>
                                <td><div >
                                <a  href="{{ route('admin.users.view', ['user_id' => $meeting_details->user_id]) }}">{{$meeting_details->user_details->username ?? tr('not_available')}}</a></div></td>
                            </tr>

                            <tr>
                                <td><b>{{ tr('meeting_members') }}</b></td>
                                <td><div >{{$meeting_details->no_of_users}}</div></td>
                            </tr>
                        
                            <tr>
                                <td><b>{{ tr('start_time') }}</b></td>
                                <td><div >{{ common_date($meeting_details->start_time, Auth::guard('admin')->user()->timezone,'H:i:s') }}</div></td>
                            </tr>

                            <tr>
                                <td><b>{{ tr('end_time') }}</b></td>
                                <td><div >{{ common_date($meeting_details->end_time, Auth::guard('admin')->user()->timezone,'H:i:s') }}</div></td>
                            </tr>

                            <tr>
                                <td><b>{{ tr('meeting_duration') }}</b></td>
                                <td><div >{{$meeting_details->call_duration}}</div></td>
                            </tr>

                            <tr>

                              <td> <b>{{ tr('status') }}</b></td>

                              <td>

                                    @if($meeting_details->status == MEETING_INITIATED)

                                        <span class="card-text label label-rouded label-menu label-warning">{{tr('meeting_initiated')}}</span>

                                    @elseif($meeting_details->status == MEETING_STARTED)

                                        <span class="card-text  label label-rouded label-menu label-success">{{tr('meeting_started')}}</span>

                                    @else

                                        <span class="card-text label label-rouded label-menu label-danger">{{tr('meeting_completed')}}</span>

                                    @endif

                              </td>

                            </tr>


                            <tr>
                                <td> <b>{{ tr('created_at') }}</b></td>
                                <td><div>{{ common_date($meeting_details->created_at, Auth::guard('admin')->user()->timezone, 'H:i:s') }}</div></td>
                            </tr>

                            <tr>
                                <td> <b>{{ tr('updated_at') }}</b></td>
                                <td><div>{{ common_date($meeting_details->updated_at,Auth::guard('admin')->user()->timezone,'H:i:s') }}</div></td>
                            </tr>

                          </tbody>

                        </table>

                    </div>

                </div>

                    <div class="grid-margin stretch-card {{ request()->type == 'view_members' ? 'view_members' : '' }}">

                        <div class="text-uppercase mb-3 text-center">
                            <h5>{{tr('meeting_members')}}</h5>
                        </div>

                        <div class="card card-shadow-remove text-center">
                                <div class="card-body">
                                        @if(count($meeting_details->members) <= 0)
                                          <h4>{{tr('no_members_found')}}</h4>
                                        @else
                                        <ul class="list-arrow">
                                        @foreach($meeting_details->members as $key=>$members)
                                            <li><h5>{{$key+1}}.  <a href="{{ route('admin.users.view', ['user_id' => $members->user_id]) }}"> {{$members->username}} </a> </h5></li>&nbsp;
                                        @endforeach
                                       </ul>
                                       @endif
                                    </div>
                            </div>	
                    </div>	

            </div>


           
								
	            				
               
					            
	            			

        </div>

    </div>

</div>

@endsection

