@extends('layouts.admin') 

@section('page_header', tr('static_pages'))

@section('breadcrumbs')

    <li class="breadcrumb-item"><a href="{{route('admin.static_pages.index')}}">{{tr('static_pages')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{tr('view_static_pages')}}</span>
    </li>
           
@endsection  

@section('content')

<div class="card text-capitalize">
    
    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('view_static_pages')}}

        </h4>

    </div>

    <div class="card-group">

        <div class="card mb-4">

            <div class="card-body">

                <div class="custom-card">
                
                    <h5 class="card-title">{{tr('title')}}</h5>
                    
                    <p class="card-text">{{$static_page_details->title}}</p>

                    <hr>

                </div> 

                <div class="custom-card">
                
                    <h5 class="card-title">{{tr('type')}}</h5>
                    
                    <p class="card-text">{{$static_page_details->type}}</p>

                    <hr>

                </div>

                <div class="custom-card">
                
                    <h5 class="card-title">{{tr('status')}}</h5>
                    
                    <p class="card-text">

                        @if($static_page_details->status == APPROVED)

                            <span class="badge badge-success badge-md text-uppercase">{{tr('approved')}}</span>

                        @else 

                            <span class="badge badge-danger badge-md text-uppercase">{{tr('pending')}}</span>

                        @endif
                    
                    </p>

                    <hr>

                </div>
                                        
                <div class="custom-card">
                
                    <h5 class="card-title">{{tr('updated_at')}}</h5>
                    
                    <p class="card-text">{{ common_date($static_page_details->updated_at, Auth::guard('admin')->user()) }}</p>

                    <hr>

                </div>

                <div class="custom-card">
                
                    <h5 class="card-title">{{tr('created_at')}}</h5>
                    
                    <p class="card-text">{{ common_date($static_page_details->created_at, Auth::guard('admin')->user()) }}</p>

                </div> 

                <hr>

                <div class="custom-card">
                
                    <div class="row">
                            
                        @if(Setting::get('is_demo_control_enabled') == NO)
                            <div class="col-md-4 col-lg-4">

                                <a href="{{ route('admin.static_pages.edit', ['static_page_id'=> $static_page_details->id] ) }}" class="btn btn-primary btn-block">{{tr('edit')}}</a>
                                
                            </div>                              

                            <div class="col-md-4 col-lg-4">
                                <a onclick="return confirm(&quot;{{tr('static_page_delete_confirmation' , $static_page_details->title)}}&quot;);" href="{{ route('admin.static_pages.delete', ['static_page_id'=> $static_page_details->id] ) }}" class="btn btn-danger btn-block">
                                    {{ tr('delete') }}
                                </a>

                            </div>                               

                        @else
                        
                            <div class="col-md-4 col-lg-4">
                                
                                <button class="btn btn-primary btn-block" disabled>{{ tr('edit') }}</button>

                            </div>
                            
                            <div class="col-md-4 col-lg-4">
                                
                                <button class="btn btn-warning btn-block" disabled>{{ tr('delete') }}</button>
                            </div>
                            
                        @endif

                        @if($static_page_details->status == APPROVED)

                            <div class="col-md-4 col-lg-4">
                                
                                <a class="btn btn-warning btn-block" href="{{ route('admin.static_pages.status', ['static_page_id'=> $static_page_details->id] ) }}" onclick="return confirm(&quot;{{ $static_page_details->title }}-{{tr('static_page_decline_confirmation' , $static_page_details->title)}}&quot;);">

                                    {{tr('decline')}}
                                </a>
                            </div>

                        @else

                            <div class="col-md-4 col-lg-4">
                                 <a class="btn btn-success btn-block" href="{{ route('admin.static_pages.status', ['static_page_id'=> $static_page_details->id] ) }}">
                                    {{tr('approve')}}
                                </a>
                            </div>
                               
                        @endif

                    </div>

                </div>

            </div>

        </div>

        <div class="card mb-4">

            <div class="card-body">

                <h4 class="card-title text-word-wrap">{{ tr('description') }}</h4>

                <p class="card-text">{!! $static_page_details->description !!}</p>
                
            </div>

        </div>

    </div>

</div>

@endsection