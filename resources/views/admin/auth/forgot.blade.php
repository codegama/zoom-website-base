@extends('layouts.admin.focused')

@section('title', 'Login')

@section('content')


<section id="wrapper">

    <body class="bg-light-gray" id="body">

        <div class="container d-flex align-items-center justify-content-center" style="min-height: 100vh ">

            <div class="d-flex flex-column justify-content-between" style="width:45%">

                <div class="row justify-content-center">

                    <div class="col-lg-12 col-md-12">

                        @include('notifications.notify')

                        <div class="card card-default mb-0">

                            <div class="card-header pb-0">

                                <div class="app-brand w-100 d-flex justify-content-center border-bottom-0">

                                    <a class="w-auto pl-0" href="#">
                                        <span class="brand-name text-dark">{{Setting::get('site_name')}}</span>
                                    </a>

                                </div>

                            </div>
                            <br>
                            <div class="card-content">

                                <div class="card-body">

                                    @include('notifications.notify')


                                    <form class="form-horizontal" role="form" method="POST" @if($is_email_configured==YES) action="{{route('admin.forgot_password.update')}}" method="POST" @endif>
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-12 control-label">{{tr('email_address')}}</label>

                                            <div class="col-md-12">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary btn-pill mb-4">
                                                    <i class="fa fa-btn fa-envelope"></i> {{tr('reset')}}
                                                </button>&nbsp;&nbsp;
                                                <a href="{{route('admin.login')}}" class="btn btn-primary btn-pill mb-4">
                                                     {{tr('login')}}
                                                </a>
                                            </div>
                                        </div>
                                    </form>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </body>

</section>

@endsection