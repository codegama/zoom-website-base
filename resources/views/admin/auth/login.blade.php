@extends('layouts.admin.focused')

@section('title', 'Login')

@section('content')

<section id="wrapper">

    <body class="bg-light-gray" id="body">
        
        <div class="container d-flex align-items-center justify-content-center" style="min-height: 100vh ">
            
            <div class="d-flex flex-column justify-content-between">
            
                <div class="row justify-content-center">
                    
                    <div class="col-lg-8 col-md-10">

                         @include('notifications.notify')
                         
                        <div class="card card-default mb-0">
                        
                            <div class="card-header pb-0">
                                
                                <div class="app-brand w-100 d-flex justify-content-center border-bottom-0">
                                    
                                    <a class="w-auto pl-0" href="#">                                    
                                        <span class="brand-name text-dark">{{Setting::get('site_name')}}</span>
                                    </a>

                                </div>

                            </div>
                            <br>
                            <div class="card-body px-5 pb-5 pt-0">
                                
                                <form action="{{ route('admin.login.post') }}" method="POST">

                                    @csrf

                                    <input type="hidden" name="timezone" id="userTimezone">
                                    
                                    <div class="row">
                                        
                                        <div class="form-group col-md-12 mb-4">
                                        
                                            <input class="form-control input-lg" type="email" required name="email" id="email" autocomplete="off" aria-describedby="emailHelp" value="{{old('email') ?? Setting::get('demo_admin_email')}}" placeholder="{{tr('email')}}">
                            
                                        </div>
                                        
                                        <div class="form-group col-md-12 ">
                                            
                                            <input class="form-control input-lg" type="password" name="password" id="password" required value="{{old('password') ?? Setting::get('demo_admin_password')}}" autocomplete="off" placeholder="{{tr('password')}}">

                                        </div>

                                        

                                        <div class="col-md-12">

                                            <center><button type="submit" class="btn btn-primary btn-pill mb-4 login-btn">{{tr('login')}}</button></center>

                                             <div class="forgot-btn">
                                           
                                              @if(Setting::get('is_demo_control_enabled') == NO)
                                              <a href="{{route('admin.password.request')}}" ><i class="ft-unlock" ></i > {{tr('forgot_password')}} </a> 
                                              @endif
                                             </div>
                                        </div>
                                    
                                    </div>
                            
                                </form>
                            </div>
                    
                        </div>
            
                    </div>
            
                </div>
        
            </div>

        </div>

    </body>

</section>


<script src="{{asset('admin-assets/js/jstz.min.js')}}"></script>


<script>
    $(document).ready(function() {

        var dMin = new Date().getTimezoneOffset();
        var dtz = -(dMin/60);
        $("#userTimezone").val(jstz.determine().name());
    });
</script>

@endsection



