@extends('layouts.admin.focused')

@section('title', 'Login')

@section('content')

<section id="wrapper">

    <body class="bg-light-gray" id="body">

        <div class="container d-flex align-items-center justify-content-center" style="min-height: 100vh ">

            <div class="d-flex flex-column justify-content-between" style="width:40%">

                <div class="row justify-content-center">

                    <div class="col-lg-12 col-md-10">

                        @include('notifications.notify')

                        <div class="card card-default mb-0">

                            <div class="card-header pb-0">

                                <div class="app-brand w-100 d-flex justify-content-center border-bottom-0">

                                    <a class="w-auto pl-0" href="#">
                                        <span class="brand-name text-dark">{{Setting::get('site_name')}}</span>
                                    </a>

                                </div>

                            </div>
                            <br>
                            <div class="card-body px-5 pb-5 pt-0">

                                <form class="form-horizontal" role="form" method="POST" action="{{route('admin.reset_password.update')}}">
                                    {{ csrf_field() }}

                                    <input type="hidden" id="reset_token" name="reset_token" value="{{Request::get('token') ?? ''}}">

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-6 control-label">{{tr('password')}}</label>

                                        <div class="form-group col-md-12 mb-4">
                                            <input id="password" type="password" class="form-control" name="password" required>

                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password-confirm" class="col-md-6 control-label">{{tr('confirm_password')}}</label>
                                        <div class="form-group col-md-12 mb-4">
                                            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>

                                            @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-refresh"></i> {{tr('reset_password')}}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </body>

</section>



@endsection